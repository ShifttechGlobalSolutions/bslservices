@extends("layouts.master")
@section("content")


    <!-- Main content Start -->
    <div class="main-content">

        <!-- Slider Start -->
        <div id="rs-slider" class="rs-slider slider3">
            <div class="bend niceties">
                <div id="nivoSlider" class="slides">
                    <img src="{{asset('public/images/slider/h2-sl1.webp')}}" alt="" title="#slide-1" />
                    <img src="{{asset('public/images/slider/h2-sl2.webp')}}" alt="" title="#slide-2" />
                </div>
                <!-- Slide 1 -->
                <div id="slide-1" class="slider-direction">
                    <div class="content-part">
                        <div class="container">
                            <div class="slider-des">
                                <div class="sl-subtitle">Sustainable Finance</div>
                                <h1 class="sl-title">We transform <br>your business </h1>
                            </div>
                            <div class="desc">Excepteur sint cupidatat non proident, sunt in coulpa qui official mollit anim id est laborum 20 years experience.</div>
                            <div class="slider-bottom ">
                                <a class="readon consultant slider" href="contact.html">Discover More</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Slide 2 -->
                <div id="slide-2" class="slider-direction">
                    <div class="content-part">
                        <div class="container">
                            <div class="slider-des">
                                <div class="sl-subtitle">Discover your business</div>
                                <h1 class="sl-title">We Promote <br>Your Business</h1>
                            </div>
                            <div class="desc">Excepteur sint cupidatat non proident, sunt in coulpa qui official mollit anim id est laborum 20 years experience.</div>
                            <div class="slider-bottom ">
                                <a class="readon consultant" href="contact.html">Discover More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Slider End -->



        <!-- About Section Start -->
        <div class="rs-about style4 bg21 pt-100 pb-100 md-pt-70 md-pb-70">
            <div class="container">
                <div class="row y-middle">
                    <div class="col-lg-6 pr-40 md-pr-15 mt-45 md-mt-0 md-mb-50">
                        <div class="sec-title3">
                            <span class="sub-text">About Us</span>
                            <h2 class="title pb-25">
                                We are crafting your unique business consulting ideas
                            </h2>
                            <div class="desc pb-17">
                                We denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms.
                            </div>
                            <p class="margin-0">
                                Today, a variety of software can create random text that resembles Lorem Ipsum. For example, Apple’s Pages and Keynote software use scrambled placeholder text. And Lorem Ipsum is featured on Google Docs, WordPress, and Microsoft Office Word. Derived from Latin dolorem ipsum, Lorem Ipsum is filler text used by publishers and graphic designers used to demonstrate graphic elements.
                            </p>
                            <div class="btn-part mt-45">
                                <a class="readon consultant discover orange-more" href="about.html">Discover More</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="about-img">
                            <img src="{{asset('public/images/about/home4/about-1.png')}}" alt="images">
                            <div class="animations one">
                                <img class="dance2" src="{{asset('public/images/about/home4/1.png')}}" alt="About">
                            </div>
                            <div class="animations two">
                                <img class="scale" src="{{asset('public/images/about/home4/2.png')}}" alt="About">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- About Section End -->

        <!-- Services Section Start -->
        <div class="rs-services main-home services-style1 home-4-style bg7 pt-95 pb-100 md-pt-70 md-pb-65">
            <div class="container">
                <div class="sec-title3 text-center mb-65 md-mb-45">
                    <span class="sub-title">Valued Services</span>
                    <h2 class="title pb-25">
                        Managed IT, Software, Voice & Data <br> Services for Your Organization.
                    </h2>
                    <div class="heading-border-line"></div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-6 mb-65">
                        <div class="services-item">
                            <div class="services-icon">
                                <img src="{{asset('public/images/services/style6/1.png')}}" alt="Services">
                            </div>
                            <div class="services-text">
                                <h2 class="title"><a href="business-planning.html">Business Planning</a></h2>
                                <p class="services-txt"> Quisque placerat vitae lacus ut scelerisque. Fusce luctus odio ac nibh luctus consulting.</p>
                                <div class="serial-number">
                                    01
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 mb-65">
                        <div class="services-item">
                            <div class="services-icon">
                                <img src="{{asset('public/images/services/style6/2.png')}}" alt="Services">
                            </div>
                            <div class="services-text">
                                <h2 class="title"><a href="tax-strategy.html">Tax Strategy</a></h2>
                                <p class="services-txt"> Quisque placerat vitae lacus ut scelerisque. Fusce luctus odio ac nibh luctus consulting.</p>
                                <div class="serial-number">
                                    02
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 mb-65">
                        <div class="services-item">
                            <div class="services-icon">
                                <img src="{{asset('public/images/services/style6/3.png')}}" alt="Services">
                            </div>
                            <div class="services-text">
                                <h2 class="title"><a href="financial-advices.html">Financial Advices</a></h2>
                                <p class="services-txt"> Quisque placerat vitae lacus ut scelerisque. Fusce luctus odio ac nibh luctus consulting.</p>
                                <div class="serial-number">
                                    03
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 md-mb-65">
                        <div class="services-item">
                            <div class="services-icon">
                                <img src="{{asset('public/images/services/style6/4.png')}}" alt="Services">
                            </div>
                            <div class="services-text">
                                <h2 class="title"><a href="insurance-strategy.html">Insurance Strategy</a></h2>
                                <p class="services-txt"> Quisque placerat vitae lacus ut scelerisque. Fusce luctus odio ac nibh luctus consulting.</p>
                                <div class="serial-number">
                                    04
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 sm-mb-65">
                        <div class="services-item">
                            <div class="services-icon">
                                <img src="{{asset('public/images/services/style6/5.png')}}" alt="Services">
                            </div>
                            <div class="services-text">
                                <h2 class="title"><a href="start-ups.html">Start Ups</a></h2>
                                <p class="services-txt"> Quisque placerat vitae lacus ut scelerisque. Fusce luctus odio ac nibh luctus consulting.</p>
                                <div class="serial-number">
                                    05
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="services-item">
                            <div class="services-icon">
                                <img src="{{asset('public/images/services/style6/6.png')}}" alt="Services">
                            </div>
                            <div class="services-text">
                                <h2 class="title"><a href="manage-investment.html">Manage Investment</a></h2>
                                <p class="services-txt"> Quisque placerat vitae lacus ut scelerisque. Fusce luctus odio ac nibh luctus consulting.</p>
                                <div class="serial-number">
                                    06
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Services Section End -->

        <!-- Services Section Start -->
        <div class="rs-services home-style2  pt-100 pb-100 md-pt-70 md-pb-70">
            <div class="container">
                <div class="sec-title2 text-center md-left mb-40">
                    <div class="sub-text">Why Choose Us</div>
                    <h2 class="title">Get our services & drive more <br><span>customers.</span></h2>
                </div>
                <div class="row y-middle">
                    <div class="col-lg-4  md-mb-50 pr-30 md-pr-l5">
                        <div class="services-item mb-45">
                            <div class="services-icon">
                                <img src="{{asset('public/images/services/style3/1.png')}}" alt="Images">
                            </div>
                            <div class="services-text">
                                <h3 class="title"><a href="services-style1.html">Expert peoples</a></h3>
                                <p class="services-txt">Quisque placerat vitae ut scelerise consulting.</p>
                            </div>
                        </div>
                        <div class="services-item mb-45">
                            <div class="services-icon">
                                <img src="{{asset('public/images/services/style3/2.png')}}" alt="Images">
                            </div>
                            <div class="services-text">
                                <h3 class="title"><a href="services-style1.html">Big experience</a></h3>
                                <p class="services-txt">Quisque placerat vitae ut scelerise consulting.</p>
                            </div>
                        </div>
                        <div class="services-item">
                            <div class="services-icon">
                                <img src="{{asset('public/images/services/style3/3.png')}}" alt="Images">
                            </div>
                            <div class="services-text">
                                <h3 class="title"><a href="services-style1.html">Financial control</a></h3>
                                <p class="services-txt">Quisque placerat vitae ut scelerise consulting.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4  md-mb-50">
                        <div class="rs-videos choose-video">
                            <div class="images-video">
                                <img src="{{asset('public/images/choose/choose-2.png')}}" alt="images">
                            </div>
                            <div class="animate-border">
                                <a class="popup-border" href="https://www.youtube.com/watch?v=FMvA5fyZ338">
                                    <i class="fa fa-play"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 pl-40 md-pl-15">
                        <div class="services-item mb-45">
                            <div class="services-icon">
                                <img src="{{asset('public/images/services/style3/4.png')}}" alt="Images">
                            </div>
                            <div class="services-text">
                                <h3 class="title"><a href="services-style1.html">Committed quality</a></h3>
                                <p class="services-txt">Quisque placerat vitae ut scelerise consulting.</p>
                            </div>
                        </div>
                        <div class="services-item mb-45">
                            <div class="services-icon">
                                <img src="{{asset('public/images/services/style3/5.png')}}" alt="Images">
                            </div>
                            <div class="services-text">
                                <h3 class="title"><a href="services-style1.html">Award winning</a></h3>
                                <p class="services-txt">Quisque placerat vitae ut scelerise consulting.</p>
                            </div>
                        </div>
                        <div class="services-item">
                            <div class="services-icon">
                                <img src="{{asset('public/images/services/style3/5.png')}}" alt="Images">
                            </div>
                            <div class="services-text">
                                <h3 class="title"><a href="services-style1.html">Insurance Policy</a></h3>
                                <p class="services-txt">Quisque placerat vitae ut scelerise consulting.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Counter Section End -->
            <div class="rs-counter style1">
                <div class="container">
                    <div class="counter-border-top">
                        <div class="row">
                            <div class="col-lg-3 col-md-6 col-sm-6 md-mb-30">
                                <div class="counter-area">
                                    <div class="counter-list mb-20">
                                        <div class="counter-icon">
                                            <img src="{{asset('public/images/counter/icons/1.png')}}" alt="Counter">
                                        </div>
                                        <div class="counter-number">
                                            <span class="rs-count">582</span>
                                        </div>
                                    </div>
                                    <div class="content-part">
                                        <h5 class="title">Projects completed for our respected clients.</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6 md-mb-30">
                                <div class="counter-area">
                                    <div class="counter-list mb-20">
                                        <div class="counter-icon">
                                            <img src="{{asset('public/images/counter/icons/2.png')}}" alt="Counter">
                                        </div>
                                        <div class="counter-number">
                                            <span class="rs-count">215</span>
                                            <span class="prefix">+</span>
                                        </div>
                                    </div>
                                    <div class="content-part">
                                        <h5 class="title">Experienced people serving to clients.</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6 xs-mb-30">
                                <div class="counter-area">
                                    <div class="counter-list mb-20">
                                        <div class="counter-icon">
                                            <img src="{{asset('public/images/counter/icons/3.png')}}" alt="Counter">
                                        </div>
                                        <div class="counter-number">
                                            <span class="rs-count">25</span>
                                            <span class="prefix">+</span>
                                        </div>
                                    </div>
                                    <div class="content-part">
                                        <h5 class="title">Years experience in business & consulting.</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6">
                                <div class="counter-area">
                                    <div class="counter-list mb-20">
                                        <div class="counter-icon">
                                            <img src="{{asset('public/images/counter/icons/4.png')}}" alt="Counter">
                                        </div>
                                        <div class="counter-number">
                                            <span class="rs-count">70</span>
                                            <span class="prefix">+</span>
                                        </div>
                                    </div>
                                    <div class="content-part">
                                        <h5 class="title">Business & consulting awards won over world.</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Counter Section End -->
        </div>
        <!-- Services Section End -->

        <!-- Project Section Start -->
        <div class="rs-project style7 gray-bg pt-100 pb-100 md-pt-70 md-pb-70">
            <div class="container custom">
                <div class="row y-middle">
                    <div class="col-lg-6">
                        <div class="sec-title2 mb-55 md-mb-30">
                            <div class="sub-text">Recent Work</div>
                            <h2 class="title mb-23">We blend business ideas to create something <span>awesome.</span></h2>
                        </div>
                    </div>
                    <div class="col-lg-6 pl-60 md-pl-15 mb-30">
                        <p class="desc mb-0">30+ years experience in business and finance consulting, IT solutions, and working with 5k+ clients over the world. Creation timelines for the digital consulting business theme 2021.</p>
                    </div>
                </div>
            </div>
            <div class="container-fluid pl-30 pr-30">
                <div class="rs-carousel owl-carousel"
                     data-loop="true"
                     data-items="4"
                     data-margin="30"
                     data-autoplay="false"
                     data-hoverpause="true"
                     data-autoplay-timeout="5000"
                     data-smart-speed="800"
                     data-dots="false"
                     data-nav="false"
                     data-nav-speed="false"

                     data-md-device="4"
                     data-md-device-nav="false"
                     data-md-device-dots="true"
                     data-center-mode="false"

                     data-ipad-device2="2"
                     data-ipad-device-nav2="false"
                     data-ipad-device-dots2="true"

                     data-ipad-device="2"
                     data-ipad-device-nav="false"
                     data-ipad-device-dots="true"

                     data-mobile-device="1"
                     data-mobile-device-nav="false"
                     data-mobile-device-dots="true">

                    <div class="project-item">
                        <div class="project-img">
                            <img src="{{asset('public/images/project/style2/4.jpg')}}" alt="images">
                        </div>
                        <div class="project-content">
                            <div class="project-inner">
                                <h3 class="title"><a href="project-single.html">Business planning</a></h3>
                                <span class="category"><a href="project-single.html">Investment</a></span>
                            </div>
                            <a class="p-icon" href="project-single.html"><i class="flaticon-right-arrow"></i></a>
                        </div>
                    </div>
                    <div class="project-item">
                        <div class="project-img">
                            <img src="{{asset('public/images/project/style2/2.jpg')}}" alt="images">
                        </div>
                        <div class="project-content">
                            <div class="project-inner">
                                <h3 class="title"><a href="project-single.html">Strength solutions</a></h3>
                                <span class="category"><a href="project-single.html">Investment</a></span>
                            </div>
                            <a class="p-icon" href="project-single.html"><i class="flaticon-right-arrow"></i></a>
                        </div>
                    </div>
                    <div class="project-item">
                        <div class="project-img">
                            <img src="{{asset('public/images/project/style2/3.jpg')}}" alt="images">
                        </div>
                        <div class="project-content">
                            <div class="project-inner">
                                <h3 class="title"><a href="project-single.html">Business analytics</a></h3>
                                <span class="category"><a href="project-single.html">Business Strategy</a></span>
                            </div>
                            <a class="p-icon" href="project-single.html"><i class="flaticon-right-arrow"></i></a>
                        </div>
                    </div>
                    <div class="project-item">
                        <div class="project-img">
                            <img src="{{asset('public/images/project/style2/4.jpg')}}" alt="images">
                        </div>
                        <div class="project-content">
                            <div class="project-inner">
                                <h3 class="title"><a href="project-single.html">Stock market analysis</a></h3>
                                <span class="category"><a href="project-single.html">Business Strategy</a></span>
                            </div>
                            <a class="p-icon" href="project-single.html"><i class="flaticon-right-arrow"></i></a>
                        </div>
                    </div>
                    <div class="project-item">
                        <div class="project-img">
                            <img src="{{asset('public/images/project/style2/5.jpg')}}" alt="images">
                        </div>
                        <div class="project-content">
                            <div class="project-inner">
                                <h3 class="title"><a href="project-single.html">Sales analysis</a></h3>
                                <span class="category"><a href="project-single.html">Financial</a></span>
                            </div>
                            <a class="p-icon" href="project-single.html"><i class="flaticon-right-arrow"></i></a>
                        </div>
                    </div>
                    <div class="project-item">
                        <div class="project-img">
                            <img src="{{asset('public/images/project/style2/6.jpg')}}" alt="images">
                        </div>
                        <div class="project-content">
                            <div class="project-inner">
                                <h3 class="title"><a href="project-single.html">Stock investments</a></h3>
                                <span class="category"><a href="project-single.html">Tax Consulting</a></span>
                            </div>
                            <a class="p-icon" href="project-single.html"><i class="flaticon-right-arrow"></i></a>
                        </div>
                    </div>
                    <div class="project-item">
                        <div class="project-img">
                            <img src="{{asset('public/images/project/style2/7.jpg')}}" alt="images">
                        </div>
                        <div class="project-content">
                            <div class="project-inner">
                                <h3 class="title"><a href="project-single.html">Advertising Technology</a></h3>
                                <span class="category"><a href="project-single.html">Business Strategy</a></span>
                            </div>
                            <a class="p-icon" href="project-single.html"><i class="flaticon-right-arrow"></i></a>
                        </div>
                    </div>
                    <div class="project-item">
                        <div class="project-img">
                            <img src="{{asset('public/images/project/style2/5.jpg')}}" alt="images">
                        </div>
                        <div class="project-content">
                            <div class="project-inner">
                                <h3 class="title"><a href="project-single.html">Business planning</a></h3>
                                <span class="category"><a href="project-single.html">Investment</a></span>
                            </div>
                            <a class="p-icon" href="project-single.html"><i class="flaticon-right-arrow"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Project Section End -->

        <!-- Testimonial Section Start -->
        <div class="rs-testimonial style2 bg10 pt-100 pb-100 md-pt-70 md-pb-70">
            <div class="container">
                <div class="sec-title2 text-center md-left mb-30">
                    <div class="sub-text">Testimonials</div>
                    <h2 class="title mb-0 white-color">Whats our customers saying<br> about us</h2>
                </div>
                <div class="rs-carousel owl-carousel" data-loop="true" data-items="3" data-margin="30" data-autoplay="true" data-hoverpause="true" data-autoplay-timeout="5000" data-smart-speed="800" data-dots="false" data-nav="false" data-nav-speed="false" data-center-mode="false" data-mobile-device="1" data-mobile-device-nav="false" data-mobile-device-dots="true" data-ipad-device="2" data-ipad-device-nav="false" data-ipad-device-dots="true" data-ipad-device2="2" data-ipad-device-nav2="false" data-ipad-device-dots2="true" data-md-device="3" data-md-device-nav="false" data-md-device-dots="true">
                    <div class="testi-wrap">
                        <div class="item-content">
                            <span><img src="{{asset('public/images/testimonial/quote.png')}}" alt="Testimonial"></span>
                            <p>Customer support is excellent and documentation good – novice can easily understand. Although I had a problem with the performance, thanks to the customer support, we have solved this problem as well.</p>
                        </div>
                        <div class="testi-content">
                            <div class="image-wrap">
                                <img src="{{asset('public/images/testimonial/avatar/1.jpg')}}" alt="Testimonial">
                            </div>
                            <div class="testi-information">
                                <div class="testi-name">David Warner</div>
                                <span class="testi-title">Envato User</span>
                                <div class="ratting-img">
                                    <img src="{{asset('public/images/testimonial/ratting.png')}}" alt="Testimonial">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="testi-wrap">
                        <div class="item-content">
                            <span><img src="{{asset('public/images/testimonial/quote.png')}}" alt="Testimonial"></span>
                            <p>Customer support is excellent and documentation good – novice can easily understand. Although I had a problem with the performance, thanks to the customer support, we have solved this problem as well.</p>
                        </div>
                        <div class="testi-content">
                            <div class="image-wrap">
                                <img src="{{asset('public/images/testimonial/avatar/2.jpg')}}" alt="Testimonial">
                            </div>
                            <div class="testi-information">
                                <div class="testi-name">Emily Blunt</div>
                                <span class="testi-title">Web Developer</span>
                                <div class="ratting-img">
                                    <img src="{{asset('public/images/testimonial/ratting.png')}}" alt="Testimonial">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="testi-wrap">
                        <div class="item-content">
                            <span><img src="{{asset('public/images/testimonial/quote.png')}}" alt="Testimonial"></span>
                            <p>Customer support is excellent and documentation good – novice can easily understand. Although I had a problem with the performance, thanks to the customer support, we have solved this problem as well.</p>
                        </div>
                        <div class="testi-content">
                            <div class="image-wrap">
                                <img src="{{asset('public/images/testimonial/avatar/3.jpg')}}" alt="Testimonial">
                            </div>
                            <div class="testi-information">
                                <div class="testi-name">Ansu Fati</div>
                                <span class="testi-title">Marketing</span>
                                <div class="ratting-img">
                                    <img src="{{asset('public/images/testimonial/ratting.png')}}" alt="Testimonial">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="testi-wrap">
                        <div class="item-content">
                            <span><img src="{{asset('public/images/testimonial/quote.png')}}" alt="Testimonial"></span>
                            <p>Customer support is excellent and documentation good – novice can easily understand. Although I had a problem with the performance, thanks to the customer support, we have solved this problem as well.</p>
                        </div>
                        <div class="testi-content">
                            <div class="image-wrap">
                                <img src="{{asset('public/images/testimonial/avatar/4.jpg')}}" alt="Testimonial">
                            </div>
                            <div class="testi-information">
                                <div class="testi-name">Angelina Jolie</div>
                                <span class="testi-title">Graphic Designer</span>
                                <div class="ratting-img">
                                    <img src="assetspublic/images/testimonial/ratting.png" alt="Testimonial">
                                </div>
                            </div>
                        </div>
                    </div>
                    {{--                    <div class="testi-wrap">--}}
                    {{--                        <div class="item-content">--}}
                    {{--                            <span><img src="assetspublic/images/testimonial/quote.png" alt="Testimonial"></span>--}}
                    {{--                            <p>Customer support is excellent and documentation good – novice can easily understand. Although I had a problem with the performance, thanks to the customer support, we have solved this problem as well.</p>--}}
                    {{--                        </div>--}}
                    {{--                        <div class="testi-content">--}}
                    {{--                            <div class="image-wrap">--}}
                    {{--                                <img src="assetspublic/images/testimonial/avatar/1.jpg" alt="Testimonial">--}}
                    {{--                            </div>--}}
                    {{--                            <div class="testi-information">--}}
                    {{--                                <div class="testi-name">David Warner</div>--}}
                    {{--                                <span class="testi-title">Envato User</span>--}}
                    {{--                                <div class="ratting-img">--}}
                    {{--                                    <img src="assetspublic/images/testimonial/ratting.png" alt="Testimonial">--}}
                    {{--                                </div>--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                    {{--                    <div class="testi-wrap">--}}
                    {{--                        <div class="item-content">--}}
                    {{--                            <span><img src="assetspublic/images/testimonial/quote.png" alt="Testimonial"></span>--}}
                    {{--                            <p>Customer support is excellent and documentation good – novice can easily understand. Although I had a problem with the performance, thanks to the customer support, we have solved this problem as well.</p>--}}
                    {{--                        </div>--}}
                    {{--                        <div class="testi-content">--}}
                    {{--                            <div class="image-wrap">--}}
                    {{--                                <img src="assetspublic/images/testimonial/avatar/2.jpg" alt="Testimonial">--}}
                    {{--                            </div>--}}
                    {{--                            <div class="testi-information">--}}
                    {{--                                <div class="testi-name">Emily Blunt</div>--}}
                    {{--                                <span class="testi-title">Web Developer</span>--}}
                    {{--                                <div class="ratting-img">--}}
                    {{--                                    <img src="assetspublic/images/testimonial/ratting.png" alt="Testimonial">--}}
                    {{--                                </div>--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                    {{--                    <div class="testi-wrap">--}}
                    {{--                        <div class="item-content">--}}
                    {{--                            <span><img src="assetspublic/images/testimonial/quote.png" alt="Testimonial"></span>--}}
                    {{--                            <p>Customer support is excellent and documentation good – novice can easily understand. Although I had a problem with the performance, thanks to the customer support, we have solved this problem as well.</p>--}}
                    {{--                        </div>--}}
                    {{--                        <div class="testi-content">--}}
                    {{--                            <div class="image-wrap">--}}
                    {{--                                <img src="assetspublic/images/testimonial/avatar/3.jpg" alt="Testimonial">--}}
                    {{--                            </div>--}}
                    {{--                            <div class="testi-information">--}}
                    {{--                                <div class="testi-name">Ansu Fati</div>--}}
                    {{--                                <span class="testi-title">Marketing</span>--}}
                    {{--                                <div class="ratting-img">--}}
                    {{--                                    <img src="assetspublic/images/testimonial/ratting.png" alt="Testimonial">--}}
                    {{--                                </div>--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                    {{--                    <div class="testi-wrap">--}}
                    {{--                        <div class="item-content">--}}
                    {{--                            <span><img src="assetspublic/images/testimonial/quote.png" alt="Testimonial"></span>--}}
                    {{--                            <p>Customer support is excellent and documentation good – novice can easily understand. Although I had a problem with the performance, thanks to the customer support, we have solved this problem as well.</p>--}}
                    {{--                        </div>--}}
                    {{--                        <div class="testi-content">--}}
                    {{--                            <div class="image-wrap">--}}
                    {{--                                <img src="assetspublic/images/testimonial/avatar/4.jpg" alt="Testimonial">--}}
                    {{--                            </div>--}}
                    {{--                            <div class="testi-information">--}}
                    {{--                                <div class="testi-name">Angelina Jolie</div>--}}
                    {{--                                <span class="testi-title">Graphic Designer</span>--}}
                    {{--                                <div class="ratting-img">--}}
                    {{--                                    <img src="assetspublic/images/testimonial/ratting.png" alt="Testimonial">--}}
                    {{--                                </div>--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                </div>
            </div>
        </div>
        <!-- Testimonial Section End -->


        <!-- Contact Section Start -->
        <div class="rs-contact contact-style2 bg11 pt-95 pb-100 md-pt-65 md-pb-70">
            <div class="container">
                <div class="sec-title2 mb-55 md-mb-35 text-center text-lg-start">
                    <div class="sub-text">Contact</div>
                    <h2 class="title mb-0">Let us help your business <br> to move <span>forward.</span></h2>
                </div>
                <div class="row y-middle">
                    <div class="col-lg-6 md-mb-50">
                        <div class="contact-img">
                            <img src="{{asset('public/images/contact/computer.jpg')}}" alt="Contact">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="contact-wrap">
                            <div id="form-messages"></div>
                            <form id="contact-form" method="post" action="https://keenitsolutions.com/products/html/bizup/mailer.php">
                                <fieldset>
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6 mb-30">
                                            <input class="from-control" type="text" id="name" name="name" placeholder="Name" required="">
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 mb-30">
                                            <input class="from-control" type="text" id="email" name="email" placeholder="E-Mail" required="">
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 mb-30">
                                            <input class="from-control" type="text" id="phone" name="phone" placeholder="Phone Number" required="">
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 mb-30">
                                            <input class="from-control" type="text" id="Website" name="subject" placeholder="Your Website" required="">
                                        </div>

                                        <div class="col-lg-12 mb-30">
                                            <textarea class="from-control" id="message" name="message" placeholder="Your message Here" required=""></textarea>
                                        </div>
                                    </div>
                                    <div class="btn-part">
                                        <div class="form-group mb-0">
                                            <input class="readon submit" type="submit" value="Submit Now">
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Contact Section End -->

    </div>
    <!-- Main content End -->


@endsection
