<!DOCTYPE html>
<html lang="en">
<head>
    <!-- meta tag -->
    <meta charset="utf-8">
    <title>Shifttech Global Solutions - Software solutions -websites -mobile apps</title>
    <meta name="description" content="Shifttech Global Solutions is a full stack software development company specializing in custom software solutions, websites and mobile apps.">
    <!-- responsive tag -->
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon -->
    <link rel="apple-touch-icon" href="apple-touch-icon.html">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('public/images/fav.png')}}">
    <!-- Bootstrap v4.4.1 css -->
    <link rel="stylesheet" type="text/css" href="{{asset('public/css/bootstrap.min.css')}}">
    <!-- font-awesome css -->
    <link rel="stylesheet" type="text/css" href="{{asset('public/css/font-awesome.min.css')}}">
    <!-- flaticon css -->
    <link rel="stylesheet" type="text/css" href="{{asset('public/fonts/flaticon.css')}}">
    <!-- animate css -->
    <link rel="stylesheet" type="text/css" href="{{asset('public/css/animate.css')}}">
    <!-- owl.carousel css -->
    <link rel="stylesheet" type="text/css" href="{{asset('public/css/owl.carousel.css')}}">
    <!-- off canvas css -->
    <link rel="stylesheet" type="text/css" href="{{asset('public/css/off-canvas.css')}}">
    <!-- magnific popup css -->
    <link rel="stylesheet" type="text/css" href="{{asset('public/css/magnific-popup.css')}}">
    <!-- Main Menu css -->
    <link rel="stylesheet" href="{{asset('public/css/rsmenu-main.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('public/inc/custom-slider/css/nivo-slider.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/inc/custom-slider/css/preview.css')}}">
    <!-- spacing css -->
    <link rel="stylesheet" type="text/css" href="{{asset('public/css/rs-spacing.css')}}">
    <!-- style css -->
    <link rel="stylesheet" type="text/css" href="{{asset('public/css/style.css')}}"> <!-- This stylesheet dynamically changed from style.less -->
    <!-- responsive css -->
    <link rel="stylesheet" type="text/css" href="{{asset('public/css/responsive.css')}}">



</head>
<body>
@include("partials.header")


@yield('content')


@include("partials.footer")

<!-- Plugins JS File -->

<!-- modernizr js -->
<script src="{{asset('public/js/modernizr-2.8.3.min.js')}}"></script>
<!-- jquery latest version -->
<script src="{{asset('public/js/jquery.min.js')}}"></script>
<!-- Bootstrap v4.4.1 js -->
<script src="{{asset('public/js/bootstrap.min.js')}}"></script>
<!-- op nav js -->
<script src="{{asset('public/js/jquery.nav.js')}}"></script>
<!-- isotope.pkgd.min js -->
<script src="{{asset('public/js/isotope.pkgd.min.js')}}"></script>
<!-- owl.carousel js -->
<script src="{{asset('public/js/owl.carousel.min.js')}}"></script>
<!-- wow js -->
<script src="{{asset('public/js/wow.min.js')}}"></script>
<!-- Skill bar js -->
<script src="{{asset('public/js/skill.bars.jquery.js')}}"></script>
<!-- imagesloaded js -->
<script src="{{asset('public/js/imagesloaded.pkgd.min.js')}}"></script>
<!-- waypoints.min js -->
<script src="{{asset('public/js/waypoints.min.js')}}"></script>
<!-- counterup.min js -->
<script src="{{asset('public/js/jquery.counterup.min.js')}}"></script>
<!-- magnific popup js -->
<script src="{{asset('public/js/jquery.magnific-popup.min.js')}}"></script>

<script src="{{asset('public/inc/custom-slider/js/jquery.nivo.slider.js')}}"></script>
<!-- contact form js -->
<script src="{{asset('public/js/contact.form.js')}}"></script>
<!-- main js -->
<script src="{{asset('public/js/main.js')}}"></script>

<!-- Main JS File -->



</body>
