<footer id="rs-footer" class="rs-footer style1">
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-12 col-sm-12 md-mb-10">
                    <div class="footer-logo mb-40">
                        <a href="index-2.html"><img src="{{asset('public/images/shift_logo.png')}}" alt=""></a>
                    </div>
                    <div class="textwidget white-color pb-40"><p>We denounce with righteous indig nation in and dislike men who are so beguiled and to demo realized by the, so blinded by desire, that they cannot foresee.</p>
                    </div>
                    <ul class="footer-social md-mb-30">
                        <li>
                            <a href="#" target="_blank"><span><i class="fa fa-facebook"></i></span></a>
                        </li>
                        <li>
                            <a href="# " target="_blank"><span><i class="fa fa-twitter"></i></span></a>
                        </li>
                        <li>
                            <a href="# " target="_blank"><span><i class="fa fa-instagram"></i></span></a>
                        </li>

                    </ul>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 md-mb-10 pl-55 md-pl-15">
                    <h3 class="footer-title">Our Services</h3>
                    <ul class="site-map">
                        <li><a href="{{ url('/services') }}">Custom Software Development</a></li>
                        <li><a href="{{ url('/services') }}">Website Development</a></li>
                        <li><a href="{{ url('/services') }}">Mobile Apps Development</a></li>

                    </ul>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 md-mb-10">
                    <h3 class="footer-title">Contact Info</h3>
                    <ul class="address-widget">
                        <li>
                            <i class="flaticon-location"></i>
                            <div class="desc">Plot 164 | Sefako Makgatho Drive , <br>
                                Kameeldrift East | Pretoria, South Africa</div>

                        </li>

                        <li>
                            <i class="flaticon-location"></i>
                            <div class="desc"> 85 Ouklipmuur Avenue | C/O Ouklipmuur Avenue & Libertas Avenue <br>
                                Equestria | Pretoria, South Africa</div>
                        </li>
                        <li>
                            <i class="flaticon-call"></i>
                            <div class="desc">

                                <a href="tel:(+27)82 822 0437">Conrad Erasmus: (+27) 82 822 0437</a>

                            </div>
                        </li>

                        <li>
                            <i class="flaticon-call"></i>
                            <div class="desc">


                                <a href="tel:(+27)814303023">Ana-Bela du Toit:  (+27) 79 877 7998</a>
                            </div>
                        </li>
                        <li>
                            <i class="flaticon-email"></i>
                            <div class="desc">
                                <a href="mailto:info@shifttechgs.com">info@bslservices.co.za</a>
                            </div>
                        </li>
                        <li>
                            <i class="flaticon-clock-1"></i>
                            <div class="desc">
                                Office Hours: 8AM - 5PM
                            </div>
                        </li>
                    </ul>
                </div>

            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row align-content-center">

                <div class="col-lg-12">
                    <div class="copyright text-lg-start text-center ">
                        <p>© 2021 BslServices . All Rights Reserved. Designed by Shifttech Global Solutions</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Footer End -->

<!-- start scrollUp  -->
<div id="scrollUp" class="orange-color">
    <i class="fa fa-angle-up"></i>
</div>
<!-- End scrollUp  -->


